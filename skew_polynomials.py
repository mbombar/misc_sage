from copy import copy


def reduction(p, m=None):
    R = p.parent()
    F = R.base_ring()
    if not m:
        m = F.degree()
    coeff = [F(0)]*m
    for (i, u) in enumerate(p.coefficients(sparse=False)):
        coeff[i%m]+=u
    return R(coeff)

def adjoint(p, m=None, verbose=False):
    """
    Take a skew polynomial p and return its adjoint.
    m is the degree of the relative extension. If not specified,
    the extension is taken over the prime subfield of the base_field
    """
    R = p.parent()
    t = p.degree()
    if not m:
        m = R.base_ring().degree()
    np = reduction(copy(p), m)
    if verbose:
        print(f"np = {np}")
    coeff = np.padded_list(m+1)
    new_coeff = list(reversed([R.twist_map(m-i)(u) for (i,u) in enumerate(coeff)]))
    D = R.gen()**m - R.gen()
    new_p = R(new_coeff)
    pstar = reduction(new_p, m)
    return pstar
